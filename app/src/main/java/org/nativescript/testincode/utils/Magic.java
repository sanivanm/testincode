package org.nativescript.testincode.utils;

import android.util.Log;

import java.util.List;

public class Magic {
    public boolean canPlayAllSpells(List<String> spells, List<String> mana) {
        int number = -1;
        for (int i = 0; i < spells.size(); i++) {
            String spell = spells.get(i);
            String[] splitSpell = spell.split("");
            for (int j = 0; j < splitSpell.length; j++) {
                Log.i("test2_incode",splitSpell[j]);
                switch (splitSpell[j]){
                    case "1":
                        number = 1;
                        break;

                    case "2":
                        number = 2;
                        break;
                    case "3":
                        number = 3;
                        break;
                    case "4":
                        number = 4;
                        break;
                    case "5":
                        number = 5;
                        break;
                    case "6":
                        number = 6;
                        break;
                    case "7":
                        number = 7;
                        break;
                    case "8":
                        number = 8;
                        break;
                    case "9":
                        number = 9;
                        break;
                    case "0":
                        number = 0;
                        break;
                }
                if (number != -1) {
                    j++;
                    for (int k = 0; k < number; k++) {
                        if (mana.contains(splitSpell[j])){
                            mana.remove(mana.indexOf(splitSpell[j]));
                            j++;
                        } else {
                            return false;
                        }
                    }
                } else {
                    if (mana.contains(splitSpell[j])){
                        mana.remove(mana.indexOf(splitSpell[j]));
                    }else{
                        return false;
                    }

                }
            }
        }
        return true;
    }
}
