    package org.nativescript.testincode;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.nativescript.testincode.utils.Magic;
import org.nativescript.testincode.utils.Necklace;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TextView txtTest, txtSecondTest;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<Integer> necklaces = new ArrayList<>();
        // index 0
        necklaces.add(6);
        // index 1
        necklaces.add(4);
        // index 2
        necklaces.add(5);
        // index 3
        necklaces.add(3);
        // index 4
        necklaces.add(1);
        // index 5
        necklaces.add(0);
        // index 6
        necklaces.add(2);
        Necklace necklace = new Necklace();
        txtTest = findViewById(R.id.txt_test);
        txtTest.setText(String.valueOf(necklace.longestNecklace(necklaces)));

        Magic magic = new Magic();
        txtSecondTest = findViewById(R.id.txt_second_test);
        ArrayList<String> spells = new ArrayList<>();
        ArrayList<String> mana = new ArrayList<>();
        spells.add("1BB");
        spells.add("1UU");
        spells.add("0");
        spells.add("1R");
        mana.add("B");
        mana.add("B");
        mana.add("W");
        mana.add("U");
        mana.add("R");
        mana.add("U");
        mana.add("B");
        mana.add("G");
        txtSecondTest.setText(magic.canPlayAllSpells(spells, mana) + "");

    }
}